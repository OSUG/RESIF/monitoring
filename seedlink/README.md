# Vérification de la cohérence des flux temps réel

Ce script
- récupère la liste des flux diffusés par les nœuds A dans les dernières minutes (par défaut 10).
- récupère la liste des canaux dans la métadonnée du nœud B
- réalise l'intersection des 2 listes
- récupère la liste des flux diffusés au nœud B
- rapporte les canaux manquant à Zabbix
