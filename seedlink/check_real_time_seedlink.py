#!/usr/bin/env python3
"""
Ce script prend en argument un code réseau

Récupère de la donnée récente par slinktool.
On compare la liste complète des channels à la liste des channels des données récentes

Envoie à Zabbix la listes des channels manquants.
"""

import sys
import re
import logging
import subprocess
import getopt
from socket import getfqdn
from datetime import timedelta, datetime
import pprint
import requests
from dateutil.parser import parse

pp = pprint.PrettyPrinter(indent=4)
# niveau de log minimum sur la sortie standard
logging.basicConfig(format="%(levelname)s: %(message)s", level=logging.INFO)

try:
    from pyzabbix.sender import ZabbixMetric, ZabbixSender
except ImportError as err:
    logging.debug("No module py-zabbix. Will not report to zabbix")


# initialisation des variables slinktool
# slinktool_server_list = ["renass-fw.u-strasbg.fr","rtserver.ipgp.fr","132.167.240.22","bud-resif-a.u-ga.fr"]
slinktool_server_list = [
    "rtserve.u-strasbg.fr",
    "132.167.240.22",
    "resif-a-rts.u-ga.fr",
    "rtserver.ipgp.fr",
]
slinktool_server_resif = ["rtserve.resif.fr"]
slinktool_port = "18000"


class Stream:
    """
    Défini un flux.
    Aucune vérification, c'est juste pour les manipuler dans ce script
    """

    def __init__(self, network, station, location, channel, lastup=0):
        self.network = network
        self.station = station
        if location == "" or location == "  ":
            self.location = "--"
        else:
            self.location = location
        self.channel = channel
        self.lastupdated = lastup

    def __repr__(self):
        return f"{self.network}_{self.station}_{self.location}_{self.channel}"

    def __eq__(self, stream):
        return bool(
            self.network == stream.network
            and self.station == stream.station
            and self.location == stream.location
            and self.channel == stream.channel
        )

    def is_from(self, net, sta):
        return bool(self.network == net and self.station == sta)


# fonctoion d'envoi des messages à zaabix
def zabbix_trapper(trapper_key, message):
    # On vérifie que les objets du module pyzabbix existent
    try:
        ZabbixMetric
    except NameError:
        logging.info("No module py-zabbix. Will not report to zabbix")
        return False

    zabbix_host = getfqdn()
    try:
        packet = [ZabbixMetric(zabbix_host, trapper_key, message)]
        result = ZabbixSender(use_config=True).send(packet)
        logging.info(result)
    except Exception as zabbix_error:
        logging.error("erreur de connexion Zabbix : %s", zabbix_error)


def metadata_network_list():
    """
    Récupère la liste des reseaux à partir du webservice station.
    """
    # date du jour utc moins 10 min au format YYYY-MM-DD
    mydate = datetime.utcnow()
    logging.debug("date du jour utc  %s", mydate)
    # webservice qui permet da'voir la liste de toutes les stations avec tous
    # les channels associés pour un resaeu donné
    logging.debug("Interrogaton de la métadonnée:")
    channel_url = (
        "http://ws.resif.fr/fdsnws/station/1/query?refreshcache&level=channel&format=text&endafter="
        + str(mydate.date())
    )
    metadata_network = set()
    # on récupere la page de la requete http
    url_content = requests.get(channel_url)
    url_content.raise_for_status()
    logging.debug("Contenu de la métadonnée: %s", url_content.text)
    logging.debug("constitution de liste metadata_network depuis la métadonnée")
    # on lit la page ligne à ligne
    for line in url_content.text.splitlines():
        if line.startswith("#"):
            continue
        # le séparateur de champs dans le fichier texte est un |
        ligne = line.split("|", 2)
        # le reseau est la premiere colone
        network = ligne[0]
        if re.match('^[A-W]', network):
            metadata_network.add(network)
            logging.debug("list of networks from metadata: %s", metadata_network)
    return list(metadata_network)


def metadata_channel_list(network, exclude_stations=[]):
    """
    Récupère la liste des channels à partir du webservice station.
    Paramètres:
    - network : un réseau pour lequel récupérer la métadonnée
    - exclude_stations: une liste de stations à exclure
    """
    # date du jour utc moins 10 min au format YYYY-MM-DD
    mydate = datetime.utcnow()
    logging.debug("date du jour utc  %s", mydate)
    # webservice qui permet da'voir la liste de toutes les stations avec tous
    # les channels associés pour un resaeu donné
    channel_url = (
        "http://ws.resif.fr/fdsnws/station/1/query?refreshcache&level=channel&format=text&endafter="
        + str(mydate.date())
        + "&network="
        + network
    )
    logging.debug("Interrogaton de la métadonnée: %s", channel_url)
    channel_list = []
    # on récupere la page de la requete http
    url_content = requests.get(channel_url)
    url_content.raise_for_status()
    logging.debug("Contenu de la métadonnée: %s", url_content.text)
    logging.debug("constitution de liste channel_list depuis la métadonnée")
    # on lit la page ligne à ligne
    for line in url_content.text.splitlines():
        if line.startswith("#"):
            continue
        # le séparateur de champs dans le fichier texte est un |
        ligne = line.split("|", 4)
        # la station est la deuxieme colonne et le channel est la quatrieme
        # colonne. on concatene les deux chanps pour avaoir une paire
        # station;channel
        channel = Stream(ligne[0], ligne[1], ligne[2], ligne[3])
        # on stocke le liste des items station;channel dans liste channel_list
        if (channel not in channel_list) and (ligne[1] not in exclude_stations):
            channel_list.append(channel)
    logging.debug("list of channels from metadata: %s", channel_list)
    return channel_list


def slinktool_channel_list(network, minutes, slinktool_server_list):
    """
    Récupérer la donnée avec slinktool
    TODO factoriser les 2 fonctions slinktool_channel_list_noeudA et slinktool_channel_list_rtserve_resif en une seule fonction : get_channels_from_ringserver(server, network, minutes=10):
    """
    channels = []
    for server in slinktool_server_list:
        slinktool = f"slinktool -Q { server }:{ slinktool_port}"
        logging.debug("lancement de la commande slinktool: %s", slinktool)
        try:
            process = subprocess.run(slinktool, shell=True, stdout=subprocess.PIPE, universal_newlines=True, timeout=5)
            logging.debug(
                "building of channel list from slinktool for network: %s", network
            )
        except subprocess.TimeoutExpired:
            logging.warning(f"Remote {server} did not respond")
            return channels


        mintime = datetime.utcnow() - timedelta(minutes=minutes)
        logging.debug("heure utc moins 10 min  %s", mintime)
        # on lit la sortie subprocess ligne à ligne
        for line in process.stdout:
            # on ne prends que les lignes  commençant par le reseau entré
            if re.search("^" + network + " ", line):
                logging.debug(line)
                # la station est le deuxieme groupe et le channel est le troisieme groupe et l'heure le groupe 5
                # L'expression régulière capture le réseau, la station, la localisation, le channel, et la date d'émission (qui peut avoir 2 formats possibles)
                # Tester ici : https://regex101.com/r/CyGbNS/1
                expression = re.search(
                    r"^([0-9A-Z]{1,2})\s+(\w+)\s+([A-Z0-9]{0,2})\s+([A-Z0-9]{3}).*([2-9][0-9]{3}[\/-][01][0-9][\/-][0-3][0-9] [0-9]{2}:[0-9]{2}:[0-9]{2}.*)$",
                    line,
                )
                # on replace les millisecondes par une chaine vide
                # on convertie le groupe 5 (heure) au fomat time
                try:
                    emission_time = parse(expression[5])
                except Exception as err:
                    logging.error("Unable to parse emission time from %s", line)
                    continue
                # on ne prend que les channels  qui ont emis ces 10  dernières minutes
                if emission_time > mintime:
                    # on concatene les deux champs pour avoir une paire station.channel
                    channel = Stream(
                        expression[1],
                        expression[2],
                        expression[3],
                        expression[4],
                        lastup=emission_time,
                    )
                    # on stocke le liste des items station;channel dans liste channels
                    if channel not in channels:
                        channels.append(channel)
    logging.debug(
        "list of channels from %s for netword %s, last %s minutes: %s",
        slinktool_server_list,
        network,
        minutes,
        channels,
    )
    return channels


def json_genarartion(networklist):
    """
    genaration de la liste des resaau sur ma sortie standard
    """
    jsonstring = '{"data":['
    for network in networklist:
        jsonstring += '{ "{#NETWORK}":"' + network + '"},'
        # remove last ','
    jsonstring = jsonstring[:-1] + "]}"
    return jsonstring


def main():
    """
    on appelle le script avec les arguments de cette façon check_near_real_time_data.py -n MQ -e LPM,PCM ou
    check_near_real_time_data.py -net MQ --exclude-sations LPM,PCM
    --net est le réseau que l'on veut controler
    --exclude-stations est la liste de stations que l'on ne cherche pas à
    --time en minutes, le temps de silence toléré
    --dryrun pour ne pas remoter à Zabbix
    contrôler car on sait qu'elles ont arrété d'emettre
    """
    # controle de format du reseau passé en argument
    listnetwork = []
    exclude_stations = []
    minutes = 10
    dryrun = False
    station = ""
    try:
        options, sys.argv[1:] = getopt.getopt(
            sys.argv[1:],
            "n:s:e:t:zvhd",
            [
                "net=",
                "exclude-stations=",
                "time=",
                "zbx-lld",
                "verbose",
                "help",
                "dryrun",
            ],
        )
        logging.debug("GETOPTIONS: %s", options)
    except getopt.GetoptError as err:
        # print help information and exit:
        print(err)  # will print something like "option -a not recognized"
        sys.exit(2)

    for opt, arg in options:
        if opt in ("-t", "--time"):
            minutes = int(arg)
        elif opt in ("-v", "--verbose"):
            logging.getLogger().setLevel(logging.DEBUG)
        elif opt in ("-n", "--net"):
            # si on entre reseau en argument, alors il doit comporter deux lettres.
            expression_net = r"[0-9A-Z]{1,2}"
            if re.search(expression_net, arg) is None:
                logging.error("Network name should have 1 or 2 capital letters")
                sys.exit(1)
            else:
                listnetwork.append(arg)
        elif opt in ("-s", "--station"):
            expression_net = r"[0-9A-Z]{3,5}"
            if re.search(expression_net, arg) is None:
                logging.error("Station name should have 3 to 5 capital chars")
                sys.exit(1)
            else:
                station = arg
        elif opt in ("-e", "--exclude-stations"):
            # si on entre une station en argument, alors il doit comprter 3 ou 4 letrres
            exc_stations_list = arg.split(",")
            for exc_station in exc_stations_list:
                expression_station = r"[0-9A-Z]{3,4}"
                if re.search(expression_station, exc_station) is None:
                    logging.error("station name should have 3 or 4 capital letters")
                    sys.exit(1)
                else:
                    exclude_stations.append(exc_station)
        elif opt in ("-z", "--zbx-lld"):
            # generation de la iste des reseaux au format json
            try:
                print(json_genarartion(metadata_network_list()))
            except requests.HTTPError as e:
                logging.error(e)
                sys.exit(1)
            sys.exit(0)
        elif opt in ("--dryrun"):
            # ne pas envoyer à Zabbix
            dryrun = True
        elif opt in ("-h", "--help"):
            print(
                """
      check_real_time_seedlink.py [-n NET] [-t 10] [-e STA1,STA2,STA3] [-v] [-z]
      """
            )
            sys.exit(0)
    if len(listnetwork) == 0:
        # si il n'y pas d'arguments en entrée du script alors va sexecuter
        # pour tous les reseaux trouvé dans la métadata
        try:
            listnetwork = metadata_network_list()
        except requests.HTTPError as e:
            logging.error(e)
            sys.exit(1)
        logging.info(
            "Missing network argument, using this list of networrk: %s", listnetwork
        )

    logging.debug("listnetwork: %s", listnetwork)
    logging.debug("exclude-stations: %s", exclude_stations)

    for network in listnetwork:
        # pour chacun des reseau, on va comparer le liste des channels recupérés par slinktool
        # initialisation de la liste de channels et des stations
        source_channels = []
        metadata_channels = []
        resif_channels = []
        filtered_channels = []
        lacking_channels = []
        logging.info("=== Scanning %s ===", network)
        # 1. On récupère la liste des channels aux nœuds A
        source_channels = slinktool_channel_list(
            network, minutes, slinktool_server_list
        )
        if station != "":
            logging.debug("On filtre pour ne garder que la station demandée")
            source_channels = [
                chan for chan in source_channels if chan.is_from(network, station)
            ]
        logging.debug("Channels au nœud A: %s", source_channels)

        # 2. On récupère la liste des métadonnées
        try:
            metadata_channels = metadata_channel_list(network, exclude_stations)
        except requests.HTTPError as e:
            logging.error(e)
            sys.exit(1)

        if station != "":
            # on filtre pour ne garder que la station demandée
            metadata_channels = [
                chan for chan in metadata_channels if chan.is_from(network, station)
            ]
        # 3. On récupère la liste du nœud B
        resif_channels = slinktool_channel_list(
            network, minutes, slinktool_server_resif
        )
        if station != "":
            # on filtre pour ne garder que la station demandée
            resif_channels = [
                chan for chan in resif_channels if chan.is_from(network, station)
            ]
        logging.debug("==> Liste des channels au nœud A: %s", source_channels)
        logging.debug(
            "==> Liste des channels dans les metadonnées: %s", metadata_channels
        )
        logging.debug("==> Liste des channels au noeud B: %s", resif_channels)
        logging.info(
            "Channels nœuds A:%d | nœud B:%d | metadata:%d",
            len(source_channels),
            len(resif_channels),
            len(metadata_channels),
        )
        # on regarde si chacun des items de la liste source_channels (venant des cinnq serveur noeudA)
        # sont dans la liste metadata_channels. Si oui on garde et on stocke dans filtered_channels
        # On filtre pour ne garder que ce qui est dans la métadonnée
        # Intersection des listes source_channels et metadata_channels
        filtered_channels = [
            value for value in source_channels if value in metadata_channels
        ]
        logging.debug("Filtered channels:  %s", filtered_channels)
        # on regarde si chacun des items de la liste filtered_channels
        # se trouve dans la liste channels (venant du nœud B)
        # si il n'y est pas on le stocke dans ma liste lacking_channels
        # On ne garde que les channels présents au nœudA et absent au nœud B
        lacking_channels = [
            value for value in filtered_channels if value not in resif_channels
        ]
        logging.debug("missing channels:  %s", lacking_channels)
        # on compte le nombre de paires station;channel manquantes
        logging.info(
            "Number of missing channels: %d/%d",
            len(lacking_channels),
            len(filtered_channels),
        )
        if len(lacking_channels) > 0:
            pp.pprint(lacking_channels)
        if not dryrun:
            # si la liste lacking_channels n'est pas vide on envoie une
            # erreur a zabbix et un warning dans les logs
            # Liste des channels manquants
            zabbix_key = f"seedlink_channels[{network}]"
            zabbix_trapper(zabbix_key, f"{lacking_channels}")
            logging.debug("zabbix check: %s %s", zabbix_key, lacking_channels)
            # Nombre de de channels manquants
            zabbix_key = f"seedlink_channels_count[{network}]"
            zabbix_trapper(zabbix_key, f"{len(lacking_channels)}")
            logging.debug("zabbix count: %s %d", zabbix_key, len(lacking_channels))


if __name__ == "__main__":
    main()
