#!/usr/bin/env /usr/bin/python3.6
"""
Ce script prend en argument un code réseau

Récupère de la donnée récente par dataselect, et compte le nombre de stations dans
la donnée téléchargée.

Envoie à Zabbix le décompte et la liste des stations.
"""

import sys
import re
import datetime
import requests
import os
import logging

from datetime import timedelta
from socket import gethostname
from obspy.clients.fdsn import Client
from obspy import UTCDateTime

# niveau de log minimum sur la sortie standard
logging.basicConfig(level=logging.DEBUG)

# zabbix trapper - fonction d'envoie des messages à Zabbix
try:
    from pyzabbix.sender import ZabbixMetric, ZabbixSender
except ImportError:
    sys.stderr.write("Module pyzabbix not found\n")
    sys.exit(1)

# fonctoion d'envoi des messages à zaabix
def zabbix_count_station_trapper(trapper_key, message):
    zabbix_host = gethostname()
    try:
        packet = [
            ZabbixMetric(zabbix_host, trapper_key, message)
                ]
        result = ZabbixSender(use_config=True).send(packet)
        logging.info(result)
    except Exception as zabbix_error:
        logging.error("erreur de connexion Zabbix :" + str(zabbix_error))

# fonction de récupration des cannaux
def list_compare(network,stations):
    mydate = datetime.datetime.now()
    station_url = 'https://ws.resif.fr/fdsnws/station/1/query?network='+network+'&level=station&format=text&starttime='+str(mydate.date())
    station_file = '/tmp/station_list.txt'
    station_list = []
    lacking_stations = []
    r = requests.get(station_url)
    open(station_file , 'wb').write(r.content)
    with open(station_file,"r") as fichier:
        for line in fichier.readlines():
            ligne=line.split('|',2)
            if (ligne[1] not in station_list) and (ligne[1] != "Station"):
                station_list.append(ligne[1])
    logging.info(f"full station list:{station_list}")
    lacking_stations = [value for value in station_list if value not in stations]
    os.remove(station_file)
    return  lacking_stations

# controle de format du reseau passé en argument
listnetwork = []
if len(sys.argv) < 2:
    logging.warning("network argument is lacking, we use this list of networrk : FR,RA,RD,ND,G,GL,WI,PF,MQ,CL")
    listnetwork = ["FR", "RA", "RD", "ND", "G", "GL", "WI", "PF", "MQ", "CL"]
else:
    expression = r"[A-Z]{1,2}"
    if re.search(expression, sys.argv[1]) is None:
        logging.error("ERROR network name should have 1 or 2 capital letters")
        sys.exit(1)
    else:
        listnetwork.append(sys.argv[1])

client = Client('RESIF')

def main():
    for network in listnetwork:
        # interval de temps de la requete
        starttime = UTCDateTime(datetime.datetime.utcnow() - timedelta(minutes=10))
        endtime = UTCDateTime(datetime.datetime.utcnow())
        # initialisation de la liste de stations
        stations = []
        lacking_stations = []
        try:
            # Récupérer la donnée (le Stream) avec Client.get_waveform()
            stream = client.get_waveforms(network, '*', '*', '*', starttime=starttime, endtime=endtime, attach_response=False)
            # on compte le nombre de stations distinctes renvoyées dans le stream
            for trace in stream.traces:
                if trace.stats.station not in stations:
                    stations.append(trace.stats.station)
            logging.info(f"network:{network} stations:{len(stations)} list:{stations}")
            # on compare les deux listes
            lacking_stations = list_compare(network,stations)
            logging.info(f"lacking_stations:{lacking_stations}")
            if len(stations) > 0:
                logging.warning("ERROR: these stations are lacking:" +str(lacking_stations))
                zabbix_key = f"check_rt_stations_{network}"
                zabbix_count_station_trapper(zabbix_key, 'ERROR: these stations are lacking:' +str(lacking_stations))
        # gestion de l erreur de stream    
        except Exception as station_error:
            logging.error("Get wavesforms ERROR :" + str(station_error))
            zabbix_count_station_trapper('check_rt_stations', 'Get wavesforms ERROR :'+str(station_error))

if __name__ == '__main__':
    main()

