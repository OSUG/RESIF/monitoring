#!/usr/bin/env /usr/bin/python3.6
"""
Ce script prend en argument un code réseau

Récupère de la donnée récente par get_wavesform. 
On compare la liste complète des channels à la liste des channels des données récentes

Envoie à Zabbix la listes des channels manquants.
"""
import sys
import re
import datetime
import requests
import os
import logging

from datetime import timedelta
from socket import gethostname
from obspy.clients.fdsn import Client
from obspy import UTCDateTime

# niveau de log minimum sur la sortie standard
logging.basicConfig(level=logging.DEBUG)

# zabbix trapper - fonction d'envoie des messages à Zabbix
try:
    from pyzabbix.sender import ZabbixMetric, ZabbixSender
except ImportError:
    sys.stderr.write("Module pyzabbix not found\n")
    sys.exit(1)

# fonctoion d'envoi des messages à zaabix
def zabbix_count_channel_trapper(trapper_key, message):
    zabbix_host = gethostname()
    try:
        packet = [
            ZabbixMetric(zabbix_host, trapper_key, message)
                ]
        result = ZabbixSender(use_config=True).send(packet)
        logging.info(result)
    except Exception as zabbix_error:
        logging.error("erreur de connexion Zabbix : %s",  str(zabbix_error))

# on compare la liste totale des channels récupérés par channel_url et la liste
# des channels récuprérés par get_wavesform. Ceci permet d'étatblir le iste des
# channel manquants
def list_compare(network,channels):
    # date du jour au format YYYY-MM-DD
    mydate = datetime.datetime.now()
    logging.debug("date du jour mydate: %s", mydate)
    # webservice qui permet da'voir la liste de toutes les stations avec tous
    # les channels associés pour un resaeu donné
    logging.debug("Interrogaton de la métadonnée:")
    channel_url = 'http://ws.resif.fr/fdsnws/station/1/query?refreshcache&level=channel&format=text&starttime='+str(mydate.date())+'&network='+network
    channel_file = '/tmp/channel_list.txt'
    channel_list = []
    lacking_channels = []
    # on récupere la page de la requete http
    url_content = requests.get(channel_url)
    logging.debug("Contenu de la métadonnée: %s", url_content.text)
    # open(channel_file , 'wb').write(r.content)
    logging.debug("constitution de liste channel_list depuis la métadonnée")
    # on lit la page ligne à ligne 
    for line in url_content.text.splitlines():
            # le séparateur de champs dans le fichier texte est un |
            ligne=line.split('|',4)
            # la station est la deuxieme colonne et le channel est la quatrieme
            # colonne. on concatene les deux chanps pour avaoir une paire
            # station;channel
            channel=ligne[1]+"."+ligne[3]
            # on stocke le liste des items station;channel dans liste channel_list
            if (channel not in channel_list) and (channel != "Station.Channel"):
                channel_list.append(channel)
    logging.info("channel_list: %s", channel_list)
    # on regarde si chacun des items de la liste channel_list (venant de
    # complete se trouve dans la liste channels (venant de get_wavesform) si il
    # n'y est pas on le stocke dans ma liste  lacking_channels
    lacking_channels = [value for value in channel_list if value not in channels]
    return lacking_channels

def main():
    # controle de format du reseau passé en argument
    listnetwork = []
    if len(sys.argv) < 2:
        # si il n'y pas d'arguments en entrée du script alors va sexecuter
        # pour tous es reseaux ("FR", "RA", "RD", "ND", "G", "GL", "WI", "PF",
        # "MQ", "CL")
        logging.warning("network argument is missing, we use this list of networrk : FR,RA,RD,ND,G,GL,WI,PF,MQ,CL")
        listnetwork = ["FR", "RA", "RD", "ND", "G", "GL", "WI", "PF", "MQ", "CL"]
    else:
        # si on entre reseau en argument, alors il doit comprter deux lettres.
        expression = r"[A-Z]{1,2}"
        if re.search(expression, sys.argv[1]) is None:
            logging.error("ERROR network name should have 1 or 2 capital letters")
            sys.exit(1)
        else:
            listnetwork.append(sys.argv[1])
    logging.debug("listnetwork: %s", listnetwork)

    client = Client('RESIF')

    for network in listnetwork:
        # pour chacun des reseau, on va comparer le liste des channels
        # recupérés par geti_wavesform durant les dix dernières minutes
        # interval de temps de la requete.
        starttime = UTCDateTime(datetime.datetime.utcnow() - timedelta(minutes=10))
        logging.debug("starttime: %s", starttime)
        endtime = UTCDateTime(datetime.datetime.utcnow())
        logging.debug("endtime: %s", endtime)
        # initialisation de la liste de channels
        channels = []
        lacking_channels = []
        try:
            # Récupérer la donnée (le Stream) avec Client.get_waveform()
            stream = client.get_waveforms(network, '*', '*', '*', starttime=starttime, endtime=endtime, attach_response=False)
            logging.debug("execution de get_waveform OK")
            # on recupère la liste des channels renvoyés par get_waveforms 
            logging.debug("building of list channels from getwaveforms")
            for trace in stream.traces:
                # on concatene les deux chanps pour avaoir une paire station;channel
                channel = trace.stats.station+"."+trace.stats.channel
                if channel not in channels:
                    channels.append(channel)
            logging.info("network: %s", network)
            logging.info("list of channels from getwaveforms: %s", channels)
            # on  recupère les paires station;channel non trouvées dans la
            # liste complète.
            lacking_channels = list_compare(network,channels)
            logging.info("missing_channels: %s", lacking_channels)
            if len(lacking_channels) > 0:
                # si il manque des channels (station;channel) on envoie une
                # erreur a zabbix et un warning dans les logs
                logging.warning("ERROR: these channels are missing: %s", str(lacking_channels))
                zabbix_key = f"check_rt_channels_{network}"
                zabbix_count_channel_trapper(zabbix_key, 'ERROR: these channels are missing:' +str(lacking_channels))
        # gestion de l erreur de stream    
        except Exception as channel_error:
            logging.error("Get wavesforms ERROR : %s", str(channel_error))
            zabbix_count_channel_trapper('check_rt_channels', 'Get wavesforms ERROR :'+str(channel_error))

if __name__ == '__main__':
    main()

