#!/usr/bin/env /usr/bin/python3.6
"""
Ce script prend en argument un code réseau

Récupère de la donnée récente par dataselect, et compte le nombre de stations dans
la donnée téléchargée.

Envoie à Zabbix le décompte et la liste des stations.
"""

import sys
import re
import datetime

from datetime import timedelta
from socket import gethostname
from obspy.clients.fdsn import Client
from obspy import UTCDateTime

# zabbix trapper - fonction d'envoie des messages à Zabbix
try:
    from pyzabbix.sender import ZabbixMetric, ZabbixSender
except ImportError:
    sys.stderr.write("Module pyzabbix not found\n")
    sys.exit(1)

def zabbix_count_station_trapper(trapper_key, message):
    zabbix_host = gethostname()
    try:
        packet = [
            ZabbixMetric(zabbix_host, trapper_key, message)
                ]
        result = ZabbixSender(use_config=True).send(packet)
        print(result)
    except Exception as zabbix_error:
        print("erreur de connexion Zabbix :" + str(zabbix_error))

# controle de format du reseau passé en argument
listnetwork = []
if len(sys.argv) < 2:
    print("network argument is lacking, we use this list of networrk : FR,RA,RD,ND,G,GL,WI,PF,MQ,CL")
    listnetwork = ["FR", "RA", "RD", "ND", "G", "GL", "WI", "PF", "MQ", "CL"]
else:
    expression = r"[A-Z]{1,2}"
    if re.search(expression, sys.argv[1]) is None:
        print("ERROR network name should have 1 or 2 capital letters")
        sys.exit(1)
    else:
        listnetwork.append(sys.argv[1])

client = Client('RESIF')

for network in listnetwork:
    # interval de temps de la requete
    starttime = UTCDateTime(datetime.datetime.utcnow() - timedelta(minutes=5))
    endtime = UTCDateTime(datetime.datetime.utcnow())
    # initialisation de la liste de stations
    stations = []
    try:
        # Récupérer la donnée (le Stream) avec Client.get_waveform()
        stream = client.get_waveforms(network, '*', '*', '*', starttime=starttime, endtime=endtime, attach_response=False)
        # on compte le nombre de stations distinctes renvoyées dans le stream
        for trace in stream.traces:
            if trace.stats.station not in stations:
                stations.append(trace.stats.station)
        # on renvoie le nombre et la liste de stations 
        print(f"network:{network} stations:{len(stations)} list:{stations}")
        zabbix_key = f"count_rt_stations_{network}"
        # zabbix_message = f"network:{network} stations:{len(stations)} list:{stations}"
        zabbix_count_station_trapper(zabbix_key, len(stations))
    # gestion de l erreur de stream    
    except Exception as station_error:
        print("Get wavesforms ERROR :" + str(station_error))
        zabbix_count_station_trapper('count_rt_stations', 'Get wavesforms ERROR :'+str(station_error))

