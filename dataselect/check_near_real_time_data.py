#!/usr/bin/env /usr/bin/python3.6
"""
Ce script prend en argument un code réseau

Récupère de la donnée récente par get_wavesform.
On compare la liste complète des channels à la liste des channels des données récentes
On compare la liste complète des stations  à la liste des stations des données récentes

Envoie à Zabbix la listes des channels manquants.
"""

import sys
import re
import datetime
import requests
import os
import logging
import getopt

from datetime import timedelta
from socket import gethostname
from obspy.clients.fdsn import Client
from obspy import UTCDateTime
import pprint
pp = pprint.PrettyPrinter(indent=4)
# niveau de log minimum sur la sortie standard
logging.basicConfig(level=logging.INFO)

try:
    from pyzabbix.sender import ZabbixMetric, ZabbixSender
except ImportError as err:
    logging.info("No module py-zabbix. Will not report to zabbix")


class Stream():
    """
    Défini un flux.
    Aucune vérification, c'est juste pour les manipuler dans ce script
    """
    def __init__(self, network, station, location, channel):
        self.network = network
        self.station = station
        if location == "" or location == "  ":
            self.location = "--"
        else :
            self.location = location
        self.channel = channel
    def __repr__(self):
        return f"{self.network}_{self.station}_{self.location}_{self.channel}"

    def __eq__(self, stream):
        return bool(self.network == stream.network and self.station == stream.station and self.location == stream.location and self.channel == stream.channel)

# fonctoion d'envoi des messages à zaabix
def zabbix_trapper(trapper_key, message):
    try:
        ZabbixMetric
    except NameError:
        return False

    zabbix_host = gethostname()
    try:
        packet = [
            ZabbixMetric(zabbix_host, trapper_key, message)
                ]
        result = ZabbixSender(use_config=True).send(packet)
        logging.info(result)
    except Exception as zabbix_error:
        logging.error("erreur de connexion Zabbix : %s" + str(zabbix_error))

# on compare la liste totale des stations récupérées par station_url et la liste
# des stations récuprérées par get_wavesform. Ceci permet d'étatblir la liste des
# channels manquants
def get_metadata_station(network, exclude_stations):
    """
    Returns a list of active stations from the station webservice
    """
    # date du jour au format YYYY-MM-DD
    mydate = datetime.datetime.now()
    logging.debug("Date du jour: %s", mydate)
    # webservice qui permet d'avoir la liste de toutes les stations pour un resaeu donné
    logging.debug("Interrogaton de la métadonnée:")
    station_url = 'https://ws.resif.fr/fdsnws/station/1/query?refreshcache&network='+network+'&level=station&format=text&starttime='+str(mydate.date())
    station_list = []
    # on récupere la page de la requete http
    url_content = requests.get(station_url)
    logging.debug("Contenu de la métadonnée: %s", url_content.text)
    logging.debug("Constitution de liste depuis la métadonnée")
    # on lit la page ligne à ligne 
    for line in url_content.text.splitlines():
        # le séparateur de champs dans le fichier texte est un |
            ligne=line.split('|',2)
            # la station est la deuxieme colonne
            station = ligne[0]+"_"+ligne[1]
            # on stocke le liste des items station;channel dans liste channel_list
            if (station not in station_list) and (ligne[1] != "Station") and (ligne[1] not in exclude_stations):
                station_list.append(station)
    logging.debug("list of stations from metadata: %s", station_list)
    return station_list

# on compare la liste totale des channels récupérés par channel_url et la liste
# des channels récuprérés par get_wavesform. Ceci permet d'étatblir la liste des
# channel manquants
def get_metadata_channels(network, exclude_stations):
    """
    Get the metadata of all active channels in a network
    """
    # date du jour au format YYYY-MM-DD
    mydate = datetime.datetime.now()
    logging.debug("date du jour mydate: %s", mydate)
    # webservice qui permet da'voir la liste de toutes les stations avec tous
    # les channels associés pour un resaeu donné
    logging.debug("Interrogaton de la métadonnée:")
    channel_url = 'http://ws.resif.fr/fdsnws/station/1/query?refreshcache&level=channel&format=text&starttime='+str(mydate.date())+'&network='+network
    channel_list = []
    # on récupere la page de la requete http
    url_content = requests.get(channel_url)
    logging.debug("Contenu de la métadonnée: %s", url_content.text)
    # open(channel_file , 'wb').write(r.content)
    logging.debug("constitution de liste channel_list depuis la métadonnée")
    # on lit la page ligne à ligne
    for line in url_content.text.splitlines():
            # le séparateur de champs dans le fichier texte est un |
            ligne=line.split('|',4)
            # la station est la deuxieme colonne et le channel est la quatrieme
            # colonne. on concatene les deux chanps pour avaoir une paire
            # station;channel
            channel = Stream(ligne[0], ligne[1], ligne[2], ligne[3])
            # on stocke le liste des items station;channel dans liste channel_list
            if (channel not in channel_list) and (ligne[3] !="Channel") and (ligne[1] not in exclude_stations):
                channel_list.append(channel)
    logging.debug("list of channels from metadata: %s", channel_list)
    return channel_list

def get_waveforms_station_channel_list(network, minutes):
    # initialisationd es donnéess
    client = Client('RESIF')
    stations = []
    channels = []
    # pour chacun des reseau, on recupère la  liste des channels et des
    # sations  par get_wavesform 
    logging.info("Getting 10 minutes of data for %s", network)
    starttime = UTCDateTime(datetime.datetime.utcnow() - timedelta(minutes=10))
    logging.debug("starttime: %s", starttime)
    endtime = UTCDateTime(datetime.datetime.utcnow())
    logging.debug("endtime: %s", endtime)
    # Récupérer la donnée (le Stream) avec Client.get_waveform()
    stream = client.get_waveforms(network, '*', '*', '*', starttime=starttime, endtime=endtime, attach_response=False)
    logging.debug("Execution of get_waveform OK")
    logging.debug("Building station list and channel list from the waveforms")
    for trace in stream.traces:
        # on récuoère les  stations distinctes renvoyées dans le stream
        station = trace.stats.network+"_"+trace.stats.station
        if station not in stations:
            stations.append(station)
            # on récupere les  channels distincts renvoyées dans le stream  
            # on concatene les deux chanps pour avaoir une paire station;channel
        channel = Stream(network, trace.stats.station, trace.stats.location, trace.stats.channel)
        if channel not in channels:
            channels.append(channel)
    logging.debug("Stations from getwaveforms: %s", stations)
    logging.debug("Channels from getwaveforms: %s", channels)
    return stations, channels

def list_to_dictionary(l):
    """
    cette fontion permet de tranformer une liste lackin_channel : ['IA2.EHN', 'IA2.EHZ',
    'LAM.HHE', 'LAM.HHN'] en un dictionnaire missing_channel_dictoary 'IA2': ['EHN', 'EHZ'], 'LAM': ['HHE', 'HHN']
    """
    logging.debug("Start convertion of list %s", l)
    dictio = {}
    # on lit les tuples de la liste un à un
    for tup in l:
        # on separe la paire station.channel en station_channel[0] = station et
        # station_channel[1] = channel
        (station, channel) = tup.split('.',2)
        if station in dictio:
            # si la clé (station) existe déja on ajoute la valeur (channel) à
            # la valeur présente
            dictio[station].append(channel)
        else:
            # si la clé (station) n'existe pas on crée une nouvelle entrée avec
            # sa valeur (channel)
            dictio[station] = [channel]
    logging.debug("List converted in dictionary %s", dictio)
    return dictio

def main():
    # controle de format du reseau passé en argument
    listnetwork = []
    exclude_stations = []
    minutes = 10
    """
    on appelle le script avec les arguments de cette façon check_near_real_time_data.py -n MQ -e LPM,PCM ou
    check_near_real_time_data.py -net MQ --exclude-sations LPM,PCM
    --net est le réseau que l'on veut controler 
    --exclude-stations est la liste de stations que l'on ne cherche pas à
    contrôler car on sait qu'elles ont arrété d'emettre
    """
    try:
        options, sys.argv[1:] = getopt.getopt(sys.argv[1:], 'n:e:t:', ['net=', 'exclude-stations=', 'time=']) 
        logging.debug("GETOPTIONS: %s", options)
    except getopt.GetoptError as err:
        # print help information and exit:
        print(err)  # will print something like "option -a not recognized"
        sys.exit(2)

    for opt, arg in options:
        if opt in ('-t', '--time'):
            minutes = int(arg)
        if opt in ('-n','--net'):
            # si on entre reseau en argument, alors il doit comprter deux lettres.
            expression_net = r"[0-9A-Z]{1,2}"
            if re.search(expression_net, arg) is None:
                logging.error("Network name should have 1 or 2 capital letters")
                sys.exit(1)
            else:
                listnetwork.append(arg)
        elif opt in ('-e','--exclude-stations'):
            # si on entre une station en argument, alors il doit comprter 3 ou 4 letrres
            exc_stations_list = arg.split(',')
            for exc_station in exc_stations_list:
                expression_station = r"[0-9A-Z]{3,4}"
                if re.search(expression_station, exc_station) is None:
                    logging.error("station name should have 3 or 4 capital letters")
                    sys.exit(1)
                else:
                    exclude_stations.append(exc_station)
        elif opt in ('-h', '--help'):
            print("""
            check_real_time_data.py [-n NET] [-t 180] [-e STA1,STA2,STA3] [-v]
            """)
            sys.exit(0)
    if len(listnetwork) == 0:
        # si il n'y pas d'arguments en entrée du script alors va sexecuter
        # pour tous es reseaux ("8C","FR", "RA", "RD", "ND", "G", "GL", "WI", "PF","MQ", "CL")
        logging.info("Missing network argument, using this list of networrk : 8C,FR,RA,RD,ND,G,GL,WI,PF,MQ,CL")
        listnetwork = ["8C","FR", "RA", "RD", "ND", "G", "GL", "WI", "PF", "MQ", "CL"]

    logging.debug("listnetwork: %s", listnetwork)
    logging.debug("exclude-stations: %s", exclude_stations)

    for network in listnetwork:
        lacking_channels = []
        lacking_stations = []
        missing_channel_dict = {}
        # on compare les deux listes de station et recupoère le liste des
        # station manquantes
        stations, channels = get_waveforms_station_channel_list(network, minutes)
        metadata_stations = get_metadata_station(network, exclude_stations)
        # on regarde si chacun des items de la liste metadata_sation
        # complete se trouve dans la liste station (venant de get_wavesform) si il
        # n'y est pas on le stocke dans ma liste  lacking_stations
        lacking_stations = [value for value in metadata_stations if value not in stations]
        logging.debug("Missing stations: %s", lacking_stations)
        # on compte le nombre de paires station;channel manquantes
        logging.info("number of missing stations:  %d/%d", len(lacking_stations), len(metadata_stations))
        if len(lacking_stations) > 0:
            # si il manque des channels (station.channel) on envoie une
            # erreur a zabbix et un warning dans les logs
            logging.warning("These stations are missing: %s", lacking_stations)
            zabbix_key = f"count_nrt_stations_{network}"
            zabbix_trapper(zabbix_key, f"{len(lacking_stations)}")
            # on presente channel manquant sous la forme d'un dictionnair
#            missing_channel_dict = list_to_dictionary(lacking_channels)
            # on envoie le contenu du dictionnaire a zabbix et un warning dans les logs
            zabbix_key = f"check_nrt_stations"
            zabbix_trapper(zabbix_key, f"{lacking_stations}")
        # on compare les deux listes de channels et recupere le listes des
        # paires de station;channel manquantes
        metadata_channels = get_metadata_channels(network, exclude_stations)
        lacking_channels = [value for value in metadata_channels if value not in channels]
        # on compte le nombre de paires station;channel manquantes
        logging.info("number of missing channels:  %d/%d", len(lacking_channels), len(metadata_channels))
        # on presente channel manquant sous la forme d'un dictionnaire
        #missing_channel_dict = list_to_dictionary(lacking_channels)
        logging.debug("Missing channels stream:  %s", lacking_channels)
        if len(lacking_channels) > 0:
            # si la liste lacking_channels n'est pas vide on envoie une
            # erreur a zabbix et un warning dans les logs
            zabbix_key = f"count_nrt_channels_{network}"
            zabbix_trapper(zabbix_key, f"{len(lacking_channels)}")
            # on presente channel manquant sous la forme d'un dictionnair
#            missing_channel_dict = list_to_dictionary(lacking_channels)
            # on envoie le contenu du dictionnaire a zabbix et un warning dans les logs
            logging.info("Missing streams: %s", lacking_channels)
            zabbix_key = f"check_nrt_channels"
            zabbix_trapper(zabbix_key, f"{lacking_channels}")
            # gestion de l erreur de stream    
            pp.pprint(lacking_channels)

if __name__ == '__main__':
    main()

