URL="http://fdsn.org/networks/detail/"
Green='\033[0;32m'        # Green
Yellow='\033[1;33m'
NC='\033[0m'
total=0
ok=0
metadata=$(wget -qO- "http://ws.resif.fr/fdsnws/station/1/query?level=network")
networks=$(echo $metadata | xmlstarlet sel -N x="http://www.fdsn.org/xml/station/1" -t -v "/x:FDSNStationXML/x:Network/@code")
echo $networks
for i in $(echo $networks); do
    echo "Analyze $i"
    resif_doi=$(echo $metadata | xmlstarlet sel -N x="http://www.fdsn.org/xml/station/1" -T -t -m "/x:FDSNStationXML/x:Network[@code='$i']" -v "./x:Identifier")
    total=$(($total + 1))
    TMPFILE=$(mktemp)
    result=0
    # Teste si la page existe
    HTTP_STATUS=$(curl -s -o $TMPFILE -L -w "%{http_code}"  $URL/$i)
    if [[ $HTTP_STATUS -ne 200 ]]; then
        echo "    - La page FDSN http://fdsn.org/networks/detail/$i n'existe pas" >> ${TMPFILE}_2
        result=1
    else
        # Teste si un DOI est publié 
        grep "doi.org" $TMPFILE > ${TMPFILE}_2
        if [[ $? -ne 0 ]]; then
            echo "    - Il n'y a pas de DOI pour $i sur  http://fdsn.org/networks/detail/$i" >> ${TMPFILE}_2
            result=1
        else 
            doi_pattern=$(echo "(10\.18715/GEOSCOPE|10\.15778/RESIF)\.$i" | tr -d '_')
            egrep -q  -e "doi.org/$doi_pattern" $TMPFILE
            if [[ $? -ne 0 ]]; then
                result=1
                echo "    - La forme du DOI est un peu louche" >> ${TMPFILE}_2
            fi
        fi
    fi
    if [[ $result -ne 0 ]]; then
        echo -e "${Yellow}=== $i ===${NC}" 
        echo -e "    ${Yellow}=> Vérifier $URL/$i${NC}"
        cat ${TMPFILE}_2
    else 
        ok=$(($ok + 1))
        echo -e "    - resif doi: $resif_doi${NC}"
        echo -e "    - fdsn doi: " 
        echo -e "${Green}=== $i OK ===${NC}"
    fi
    rm ${TMPFILE} ${TMPFILE}_2
done

echo "Résultat du test : ${ok}/${total} réseaux OK"

