#!/usr/bin/env python3
from overlaps import overlaps

def test_get_a_node():
    assert overlaps.get_a_node("RA_LIQF_01_HN2") == "RAP"
    assert overlaps.get_a_node("FR_LIQF_01_HN2") == "RAP"
    assert overlaps.get_a_node("FR_LIQF_01_HH2") == "RLBP"
    assert overlaps.get_a_node("MT_LIQF_01_HH2") == "RLBP"
    assert overlaps.get_a_node("WI_LIQF_01_HH2") == "Volcano"
