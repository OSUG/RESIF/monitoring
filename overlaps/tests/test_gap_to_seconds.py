#!/usr/bin/env python3

from overlaps import overlaps

def test_gaps_to_seconds():
    assert isinstance( overlaps.gap_to_seconds("-1.0d"), float)
    assert isinstance( overlaps.gap_to_seconds("-1.0h"), float)
    assert isinstance( overlaps.gap_to_seconds("30000"), float)
