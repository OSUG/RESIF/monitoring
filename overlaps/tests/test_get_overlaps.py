#!/usr/bin/env python3

from overlaps import overlaps

def test_get_overlaps():
    gap = overlaps.get_overlap('RA_LIQF_01_HN2', 'tests/overlaps.txt')
    print(gap)
    assert gap < 0

def test_get_overlaps_no_source():
    gap = overlaps.get_overlap('RD_LIQF_01_HN2', 'tests/overlaps.txt')
    print(gap)
    assert gap == 0
