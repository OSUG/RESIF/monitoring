#!/usr/bin/env python3
from os.path import isfile
from overlaps import overlaps

def test_save_overlaps():
    overlaps.save_overlaps({"RA_LIQF_01_HN2":-3000}, threshold=-300, output_file='tests/overlaps.out')
    assert isfile('tests/overlaps.out')
