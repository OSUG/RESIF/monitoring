#!/usr/bin/env python3
import json

from overlaps import overlaps

def test_zabbix_lld():
    lld = overlaps.zabbix_lld('tests/overlaps.txt')
    assert json.loads(lld)
