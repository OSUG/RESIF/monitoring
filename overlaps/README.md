# Monitoring overlaps in real time buffer

This tool analize overlaps in a raw data directory (miniseed files). Group overlaps by sources and store them in a result file.

The system is meant to integrate nicely with Zabbix

## Deployment

Install with pip from this local package repository:

   pip install overlaps --index-url https://gricad-gitlab.univ-grenoble-alpes.fr/api/v4/projects/7020/packages/pypi/simple 
   
   
## Usage

The tool installs a console script with full documentation. Try it :

  overlaps
  
## Zabbix integration

In order to integrate with a Zabbix server, make sure you have a local zabbix agent well configured.

Import the template from this repository (`zabbix_template_overlaps.xml`) in the server GUI.

Configure the zabbix agent by adding the following parameters (adapt the paths as needed)

  UserParameter=overlaps.all,/var/lib/zabbix/.local/bin/overlaps get_all -f /var/log/zabbix/overlaps.out
  UserParameter=overlaps.discovery,/var/lib/zabbix/.local/bin/overlaps zbxlld -f /var/log/zabbix/overlaps.out
  UserParameter=overlaps.get[*],/var/lib/zabbix/.local/bin/overlaps get -f /var/log/zabbix/overlaps.out $1 
