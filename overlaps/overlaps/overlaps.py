#!/usr/bin/env python3
from os import path, chmod
import glob
import re
from subprocess import run as subrun
import logging
import json
from datetime import datetime
import click
import click_log

logger = logging.getLogger(__name__)
click_log.basic_config(logger)

def get_a_node(source_id):
    """
    Cette fonction renvoie le nom du nœud A en fonction du source_id
    Pour le moment, ce n'est pas 100% fiable mais cela devrait fonctionner en général.
    """
    a_node = "RAP"
    (net,sta,loc,cha) = source_id.split('_')
    if net in ['WI','PF','GL','MQ','QM']:
        a_node = "Volcano"
    elif net == 'G':
        a_node = "Geoscope"
    elif net == 'RD' and cha[1] != 'N':
        a_node = "Dase"
    elif net in ['FR','ND','MT','CL','FO'] and cha[1] != 'N':
        a_node = 'RLBP'
    elif re.match('^[XYZ0-9].', net):
        a_node = 'Sismob'

    return a_node


def get_overlap(source_id, input_file):
    """
    Cette fonction renvoie la quantité d'overlap en secondes pour une source.
    Pour cela on ouvre le fichier des overlaps.
    params:
      source_id: Texte de source identifier (NET_STA_LOC_CHA)
      input_file: par défaut, le fichier passé en environnement. Sinon, un chemin vers un fichier contenant les overlaps
    returns; integer, the overlap
    """
    gap = 0
    try:
        with open(input_file, 'r') as of:
            for line in of:
                if re.search(source_id, line):
                    gap = int(line.strip().split(' ')[-1])
                    break
    except Exception as e:
        logger.error(f"Unable to open file {input_file}")
        raise e
    return gap

def get_all_overlaps(input_file):
    """
    Cette fonction renvoie la quantité d'overlap total en secondes.
    Pour cela on ouvre le fichier des overlaps.
    params:
      input_file: par défaut, le fichier passé en environnement. Sinon, un chemin vers un fichier contenant les overlaps
    returns; integer, the overlap
    """
    gap = 0
    try:
        with open(input_file, 'r') as of:
            for line in of.readlines():
                if not line.startswith('#'):
                    gap += int(line.strip().split(' ')[-1])
    except Exception as e:
        logger.error(f"Unable to open file {input_file}")
        raise e
    return gap

def gap_to_seconds(gap):
    """
    A gap can be postfixed with a letter (h, d). Transform to seconds.
    Returns a float
    """
    seconds = 0
    if gap.endswith('h'):
        seconds = float(gap[:-1])*3600
    elif gap.endswith('d'):
        seconds = float(gap[:-1])*3600*24
    else:
        try:
            seconds=float(gap)
        except ValueError as e:
            raise e
    logger.debug(f"Computed gap: {gap}")
    return seconds

def compute_overlaps(data_dir):
    """
    Cette fonction analyse les overlaps dans les données brutes
    Renvoie un dictionnaire {source: overlap}
    """
    overlaps = {}
    output = ""
    data_files = [ f for f in glob.glob(f"{data_dir}/**", recursive=True) if  path.isfile(f) ]

    for f in data_files:
        logger.debug(f"Scanning {f} for gaps")
        p = subrun( ['msi', '-tg', f], capture_output=True)
        output += p.stdout.decode()

    for line in output.split('\n'):
        logger.debug(f"Line: {line}")
        if re.search(r" -[0-9]+", line):
            (source, _, _, gap, _, _) = line.split()
            gap = gap_to_seconds(gap)
            logger.debug(f"Found overlap in {source} : {gap}")
            if source not in overlaps.keys():
                overlaps[source] = gap
            else:
                overlaps[source] += gap
    logger.debug(overlaps)
    return overlaps

def save_overlaps(overlaps, output_file, threshold=-150):
    """
    Save overlaps in a file. Format is one overlap per line
    NET_STA_LOC_CHA value
    params:
    - overlaps : a dictionary containing {source: overlap} entries, as returned by compute_overlaps() function
    """
    logger.debug(f"Write overlaps in {output_file} under threshold of {threshold}")
    with open(output_file, 'w') as outfile:
        for src, o in overlaps.items():
            if o < threshold:
                outfile.write(f"{src}    {int(o)}\n")
        outfile.write(f"# Generated at {datetime.now()}")


def zabbix_lld(input_file):
    """
    Cette fonction renvoie la liste des items que Zabbix doit créer
    """
    lld_out = []
    with open(input_file, 'r') as of:
        for line in of:
            if line.startswith('#'):
                continue
            source_id = line.split(' ')[0]
            lld_out.append({"{#SOURCEID}": source_id, "{#ANODE}": get_a_node(source_id)})
    logger.debug(json.dumps(lld_out))
    return json.dumps(lld_out)

@click.command(help="Print a list of items for Zabbix LLD")
@click.pass_context
def zbxlld(ctx):
    print(zabbix_lld(ctx.obj['file']))

@click.command(help="Analyse the gaps in the raw data")
@click.option('--datadir', '-d', help="Directory containing the data to parse", default='~/work/out/')
@click.option('--threshold', '-t', help="Threshold to keep gaps. A negative number", default=-150)
@click.pass_context
def run(ctx, datadir, threshold):
    overlaps = compute_overlaps(datadir)
    save_overlaps(overlaps, threshold=threshold, output_file=ctx.obj['file'] )
    chmod(ctx.obj['file'], 0o644)

@click.command(help="Get the overlap for a specific source (NET_STA_LOC_CHA)")
@click.pass_context
@click.argument("source")
def get(ctx, source):
    print(get_overlap(source, ctx.obj['file']))

@click.command(help="Get the sum for all the overlaps")
@click.pass_context
def get_all(ctx):
    print(get_all_overlaps(ctx.obj['file']))

@click.group()
@click.option('--file', '-f', help="Output file for the computed gaps", default='/tmp/overlaps.out', type=click.Path(exists=True))
@click_log.simple_verbosity_option(logger)
@click.pass_context
def cli(ctx, file):
    ctx.ensure_object(dict)
    ctx.obj['file'] = file


cli.add_command(zbxlld)
cli.add_command(run)
cli.add_command(get)
cli.add_command(get_all)


if __name__ == '__main__':
    cli(obj={})
